# puppeteer-heroku-buildpack

Installs deps for puppeteer on heroku.
Must have `--no-sandbox` set.

## Usage

```sh-session
$ heroku buildpacks:set https://adam-book-runners@bitbucket.org/adam-book-runners/puppeteer-buildpack.git
```
